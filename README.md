# Grafana stack

This repo contains full Grafana stack in docker.

How to get running:

1. `git clone git@gitlab.com:pbcom_bobac/grafana.git`
2. `cd grafana`
3. rename `grafana_variables.env.example` to `grafana_variables.env`
4. set valiables in `grafana_variables.env` to fit your needs (prepopulated are those to get mail working)
5. `./create_data_dir.sh` - at least on Ubuntu 16.04, creates directory for Grafana, creates user grafana with uid 472 and chowns the directory to avoid permission problems. Bit a hack but don't know how to do better :-) Not necessary on MacOS.
6. `docker-compose up`
7. alter `telegraf.conf`
8. navigate to http://localhost:3000
9. user/pass: admin/admin
10. datasource InfluxDB is on http://influxdb:8086, database telegraf, user telegraf, no pass (database is created by running telegraf container)

## Notes
1. example dashboards are in dashboards/ directory - need altering
2. telegraf container is set up with snmp tools and config to get snmp monitoring out of the box, just edit the [[inputs.snmp]] agents section.
3. consider adding of `GF_SERVER_ROOT_URL=http://youripordomain:3000` to `grafana_variables.env`. Usefull for reverse-proxying and inviting of the users.
